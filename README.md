

# keycloak-event-gateway-screenshots

These are some
screenshots captured by the integration tests from 
the [keycloak-event-gateway](https://github.com/kalisio/keycloak-event-gateway/) project.


| Test | Description | Elapsed Time | Screenshots |
| :-- | :-- | :--: | :--: |
| [`integration_tests.js`](https://github.com/kalisio/keycloak-event-gateway/blob/dfccdb773b9cff64244fd1993f939994157e7624/test/integration_tests.js) | browses Keycloak config | 22 sec | [14](screenshots/integration_tests/index.md) |	
