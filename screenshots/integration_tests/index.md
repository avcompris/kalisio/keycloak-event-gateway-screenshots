

_[Back to the home page](../../README.md)_




# Project: keycloak-event-gateway



## Test: integration_tests.js


### Description




 
> browses Keycloak config

### Steps (14) — 22 sec
1. [Login page](00000001.md)
2. [Credentials](00000002.md)
3. [Submit the login form](00000003.md)
4. [Deploy the realm list](00000004.md)
5. [Add a realm](00000005.md)
6. [Fill in the realm form](00000006.md)
7. [Submit the realm form](00000007.md)
8. [Go to the realm settings](00000008.md)
9. [Open the Events tab](00000009.md)
10. [Open the popup listbox](00000010.md)
11. [Select: "keycloak-event-gateway" in the listbox](00000011.md)
12. [Deploy the action menu](00000012.md)
13. [Ask to delete the realm](00000013.md)
14. [Confirm the realm deletion](00000014.md)


### Screenshots


|  **1. Login page** | **2. Credentials** | **3. Submit the login form** | **4. Deploy the realm list** |
| :--: | :--: | :--: | :--: |
|  [![00000001.png](./00000001.png)](00000001.md) | [![00000002.png](./00000002.png)](00000002.md) | [![00000003.png](./00000003.png)](00000003.md) | [![00000004.png](./00000004.png)](00000004.md) |
|  **5. Add a realm** | **6. Fill in the realm form** | **7. Submit the realm form** | **8. Go to the realm settings** |
|  [![00000005.png](./00000005.png)](00000005.md) | [![00000006.png](./00000006.png)](00000006.md) | [![00000007.png](./00000007.png)](00000007.md) | [![00000008.png](./00000008.png)](00000008.md) |
|  **9. Open the Events tab** | **10. Open the popup listbox** | **11. Select: "keycloak-event-gateway" in the listbox** | **12. Deploy the action menu** |
|  [![00000009.png](./00000009.png)](00000009.md) | [![00000010.png](./00000010.png)](00000010.md) | [![00000011.png](./00000011.png)](00000011.md) | [![00000012.png](./00000012.png)](00000012.md) |
|  **13. Ask to delete the realm** | **14. Confirm the realm deletion** |
|  [![00000013.png](./00000013.png)](00000013.md) | [![00000014.png](./00000014.png)](00000014.md) |